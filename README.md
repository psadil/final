
# final

This is an `R` package created for the analyses. An `R` package provides
a convenient way to package Stan code. It can be installed by opening up
the final.Rproj file in Rstudio. Then, under the ‘Build’ tab at the top,
selecting the option ‘Clean and Rebuild.’ That will compile the Stan
models and install the package. Models only need to be compiled once.

NOTE: all cached objects now actually stored in `data-raw/cache_final`,
regardless of what the remainder of the README says (there is no .drake
folder). The actually cached files are stored with git-lfs.

## Structure of Package

`R` code is stored in the subfolder ‘R.’ The two most important files
are “gen\_data.R” and “fit.R”, which contains code to simulate data and
fit the Stan model to that data, respectively. The `Stan` code is
contained in ‘src/stan\_code.’ The single participant model is in the
file “ddm\_fixed.stan” and the multi-participant model is
“ddm\_mixed.stan.”

The final report itself is in the file “vignettes/main.Rmd.” It is an
Rmd document which, when knit, will produce the figures that went into
the project proposal.

Finally, there is a hidden foler “.drake” which contains the cache of
all analyses (see next session). Presence of a cache enables the
“main.Rmd” document to be knit without rerunning all analyses.

Note that this entire package has been version controlled on gitlab, and
so could also be installed by calling the following line in `R`

``` r
install.packages("remotes")
remotes::install_gitlab("psadil/final")
```

## Running analyses

Because the analyses take so long to run (and occasionally crashed), it
was useful to have a good cache and parallelization system. The package
`drake` provides both of these. In the following, the function
`init_power_wf` (from this package) is used to generate a workflow. The
workflow is a data.frame object. Each row is a target (e.g., simulated
dataset, chain that was fit with Stan, collection of fits) that will
need to be generated, and the second column will contain the R command
needed to generate that target.

The following lines of `R` could be used to recreate that cache.
However, there ought to be a folder ‘.drake’ (see above section) which
already contains all of the files.

``` r
library(final)
library(drake)
wf1 <- final::init_power_wf(n=c(1000,2000),
                    n_subject = 1,
                    iter = 2000,
                    warmup = 1000,
                    n_chain = 10,
                    ndts = list(c(0.3, 0.35)),
                    drifts = list(c(2, 1)),
                    sv = 0.5, st0 = 0.1, a=1, v=2,
                    sigma_drift_location = 0, sigma_ndt_location = 0,
                    sigma_bs = 0, sigma_bias=0,
                    model = "ddm_fixed",
                    pars = c("drift_z","ndt_raw","ndt_z","bias_z",
                             "ndt","drift","bs_z","bias_z",
                             "ndt_location_z","drift_location_z"), include=FALSE,
                    priors = c(5, 5, 2, 0.5, 3, 0.5, 0.5),
                    prior_only = FALSE,
                    control = list(adapt_delta = 0.99, max_treedepth=12))
wf2 <- final::init_power_wf(n=250,
                     n_subject = 5,
                     iter = 2000,
                     warmup = 1000,
                     n_chain = 10,
                     ndts = list(c(0.3, 0.35)),
                     drifts = list(c(2, 1)),
                     sv = 0.5, st0 = 0.1, a=1, v=2,
                     sigma_drift_location = 0.2, sigma_ndt_location = 0.2,
                     sigma_bs = 0.05, sigma_bias=0.05,
                     model = "ddm_mixed",
                     pars = c("drift_z","drift","ndt","ndt_z",
                              "bias","bias_z","bs_z", "bs","ndt_raw",
                              "drift_location_z","ndt_location_z",
                              "drift_location","ndt_location"), include=FALSE,
                     priors = c(5, 5, 2, 0.5, 3, 0.5, 0.5),
                     prior_only = FALSE,
                     control = list(adapt_delta = 0.99, max_treedepth=12))

wf <- rbind(wf1, wf2)
```

In the following, increase the value of `targets=1` to run multiple
cores in parallel. Note that each individual chain can occasionally
require a huge amount of RAM. These analyses were actually run at the
MGHPCC cluster, which enabled running 120 chains in parallel.

``` r
options(clustermq.scheduler = "multicore")
drake::make(wf, verbose=4, jobs = c(imports=1, targets = 1), parallelism = "clustermq")
```
