data {
  real scale;
  real loc;
  int n;
  real sigma;
}
transformed data{
  vector[n] L = rep_vector(0.0, n);
  vector[n] U = rep_vector(.4, n);
}
parameters {
 // real loc;
 // real<lower=0> scale;
 vector<lower=0,upper=1>[n] mu_raw;
 // vector[n] mu;
}
transformed parameters{
  // vector[n] LL = (L-loc)/scale;
  // vector[n] UU = (U-loc)/scale;
  vector[n] mu = L+(U-L).*mu_raw;
  // vector[n] mu = L+(U-L).*inv_logit(mu_raw);
  // vector[n] mu = LL+(UU-LL).*mu_raw;
  // vector[n] mu1 = (mu*scale + loc);
}
model {
  // loc ~ normal(.4,.1);
  // scale ~ normal(0,sigma);
  target += normal_lpdf(mu|loc,scale);
  target += log(U-L);
  for(i in 1:n){
    target += log_diff_exp(normal_lcdf(U[i] | loc, scale), normal_lcdf(L[i] | loc, scale));
  }
}
