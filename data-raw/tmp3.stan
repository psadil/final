//
// This Stan program defines a simple model, with a
// vector of values 'y' modeled as normally distributed
// with mean 'mu' and standard deviation 'sigma'.
//
// Learn more about model development with Stan at:
//
//    http://mc-stan.org/users/interfaces/rstan.html
//    https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started
//
data {
}
transformed data{
  real L = 0.0;
  real U = 10;
  // real scale = .1;
}
parameters {
 real<lower=0, upper=.2> loc;
 real mu_raw;
 real<lower=0> scale;
}
transformed parameters{
  real LL = (L-loc)/scale;
  real UU = (U-loc)/scale;
  real<lower=LL, upper=UU> mu_z = LL + (UU - LL)*inv_logit(mu_raw);
}
model {
  scale ~ std_normal();
  mu_z ~ normal(loc, scale) T[LL, UU];
  // target += -log_diff_exp(normal_lcdf(UU | loc, scale), normal_lcdf(LL | loc, scale));
  target += log(UU - LL)+log_inv_logit(mu_raw) + log1m_inv_logit(mu_raw);
}
