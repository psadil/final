data {
  real scale;
  real loc;
  int n;
  real sigma;
}
transformed data{
  vector[n] L = rep_vector(0.0, n);
  vector[n] U = rep_vector(.4, n);
}
parameters {
 // real loc;
 // real<lower=0> scale;
 vector<lower=0,upper=1>[n] mu_raw;
}
transformed parameters{
  vector[n] mu = L+(U-L).*mu_raw;
}
model {
  // loc ~ normal(.2,.05);
  // scale ~ normal(0,sigma);
  mu ~ normal(loc,scale);
  for(i in 1:n){
    // mu[i] ~ normal(loc,scale) T[L[i],U[i]];
    // target += -log_diff_exp(normal_lcdf(U[i]|loc,scale),normal_lcdf(L[i]|loc,scale));
  }
}
