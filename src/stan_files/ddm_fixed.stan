data {
  int<lower=1> n;
  vector[n] rt;
  int response[n];
  int<lower=1> n_condition;
  int condition[n];
  vector[7] priors;
  int prior_only; // should the likelihood be ignored?
}
transformed data{
  real eps = sqrt(machine_precision());
  vector[n] rt_meps = rt - eps;
}
parameters {
  vector[n_condition] drift_location; // average drift rate
  real<lower=0> drift_scale; // average variability in drift (across trials)
  vector[n] drift_z; // drift rate on each trial
  vector[n_condition]  ndt_location; // average non-decision time in each condition)
  real<lower=0> ndt_scale; // scale of non-decision time
  vector<lower=0,upper=1>[n] ndt_raw; // tmp variable to allow censoring
  real<lower=0> bs; // boundary separation
  real<lower=0, upper=1> bias; // bias to respond to one value or the other
}
transformed parameters{
  vector[n] drift = drift_z*drift_scale + drift_location[condition];
  vector[n] ndt = rt_meps .* ndt_raw;
}
model {
  bs ~ gamma(4, priors[1]);
  bias ~ beta(priors[2], priors[2]);

  ndt_location ~ normal(0, priors[4]);
  ndt_scale ~ normal(0, priors[6]);
  drift_scale ~ normal(0, priors[3]);
  drift_location ~ normal(0, priors[5]);

  drift_z ~ std_normal();
  ndt ~ normal(ndt_location[condition], ndt_scale);
  target += -normal_lccdf(0 | ndt_location[condition], ndt_scale); // truncation below at 0 (censoring handled implicitly)

  if (!prior_only){
    vector[n] beta;
    vector[n] delta = drift;
    for(i in 1:n){
      if(response[i]==1){
        beta[i] = bias;
      }else{
        beta[i] = 1-bias;
        delta[i] *= -1;
      }
    }
    rt ~ wiener(bs, ndt,  beta, delta);
  }
}
