data {
  int<lower=1> n;
  vector[n] rt;
  int response[n];
  int<lower=1> n_condition;
  int condition[n];
  int n_subject;
  int subject[n];
  vector[7] priors;
  int prior_only; // should the likelihood be ignored?
}
transformed data{
  real eps = sqrt(machine_precision());
  vector[n] rt_meps = rt - eps;
}
parameters {
  real<lower=0> drift_scale; // average variability in drift (across trials)
  real<lower=0> ndt_scale; // scale of non-decision time
  vector<lower=0, upper=1>[n] ndt_raw;
  real<lower=0> sigma_drift_location;
  real<lower=0> sigma_ndt_location;
  vector[n_condition] mu_drift_location;
  vector[n_condition] mu_ndt_location;
  vector[n_subject] drift_location_z;
  vector[n_subject] ndt_location_z;
  vector[n] drift_z;
  real<lower=0> sigma_bs;
  real<lower=0> sigma_bias;
  real<lower=0, upper=1> mu_bias;
  real<lower=0> mu_bs;
  vector<lower=-mu_bias/sigma_bias, upper=(1-mu_bias)/sigma_bias>[n_subject] bias_z;
  vector<lower=-mu_bs/sigma_bs>[n_subject] bs_z;
}
transformed parameters{
  vector[n_subject] drift_location = drift_location_z*sigma_drift_location;
  vector[n_subject] ndt_location = ndt_location_z*sigma_ndt_location;
  vector[n] drift = drift_z*drift_scale + drift_location[subject] + mu_drift_location[condition];
  vector[n] ndt = rt_meps .* ndt_raw;
  vector<lower=0, upper=1>[n_subject] bias = bias_z*sigma_bias + mu_bias;
  vector<lower=0>[n_subject] bs = bs_z*sigma_bs + mu_bs;
}
model {
  sigma_bs ~ normal(0, priors[7]);
  sigma_bias ~ normal(0, priors[7]);
  mu_bias ~ beta(priors[2], priors[2]);
  mu_bs ~ gamma(4, priors[1]);
  bias_z ~ std_normal();
  bs_z ~ std_normal();
  target += -normal_lccdf(-mu_bs/sigma_bias | 0, 1)*n_subject;
  target += -log_diff_exp(normal_lcdf((1-mu_bias)/sigma_bias | 0, 1),
                          normal_lcdf(-mu_bias/sigma_bias | 0, 1))*n_subject;

  sigma_drift_location ~ normal(0, priors[7]);
  sigma_ndt_location ~ normal(0, priors[7]);
  mu_drift_location ~ normal(0, priors[5]);
  mu_ndt_location ~ normal(0, priors[4]);

  ndt_location_z ~ std_normal();
  drift_location_z ~ std_normal();

  ndt_scale ~ normal(0, priors[6]);
  drift_scale ~ normal(0, priors[3]);

  drift_z ~ std_normal();
  ndt ~ normal(ndt_location[subject] + mu_ndt_location[condition], ndt_scale);
  target += -normal_lccdf(0 |ndt_location[subject] + mu_ndt_location[condition], ndt_scale); // add back upper bound of probs (only censored above rt)

  if (!prior_only){
    vector[n] beta = bias[subject];
    vector[n] delta = drift;
    for(i in 1:n){
      if(response[i]==0){
        beta[i] = 1-bias[subject[i]];
        delta[i] *= -1;
      }
    }
    rt ~ wiener(bs[subject], ndt,  beta, delta);
  }
}
