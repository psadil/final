#' run the model
#'
#' most parameters passed on to rstan::sampling
#'
#' @param stan_data from compose_standata
#' @param model model from this package to sample from (either "ddm_mixed" or "ddm_fixed")
#' @param ... see rstan::sampling
#'
#' @export
run_stan <- function(stan_data,
                     model = "ddm_fixed",
                     chains = 4,
                     ...) {

  post <- rstan::sampling(
    object = stanmodels[model][[1]],
    data = stan_data,
    chains = chains,
    ...
  )

  return(post)
}

#' prepare data for fitting stna model
#'
#' @param d tbl (output from gen_data)
#' @param priors vector of priors to use
#' @param prior_only boolean, whether to sample from just the prior distribution
#'
#' @return list of values for sending to  stan
#'
#' @export
compose_standata <- function(d,
                             priors = c(5, 5, 2, 0.5, 3, 0.5, 0.5),
                             prior_only = FALSE) {

  stan_data <- d %>%
    tidybayes::compose_data() %>%
    c(.,
      priors = list(priors),
      prior_only = prior_only,
      min_rt = d %>% dplyr::group_by(condition) %>% dplyr::summarise(min_rt = min(rt)) %>% magrittr::use_series(min_rt) %>% list(),
      max_rt = d %>% dplyr::group_by(condition) %>% dplyr::summarise(max_rt = max(rt)) %>% magrittr::use_series(max_rt) %>% list()
    )

  return(stan_data)
}
